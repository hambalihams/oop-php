<?php
    require "Animal.php";
    require "Frog.php";
    require "Ape.php";

    $sheep = new Animal("shaun");

    echo $sheep->get_name(); // "shaun"
    echo "<br>";
    echo $sheep->get_legs(); // 2
    echo "<br>";
    echo $sheep->get_cold_blooded(); // false

    echo "<br><br>";
    
    $sungokong = new Ape("kera sakti");
 
    echo $sungokong->get_name();
    echo "<br>";
    echo $sungokong->get_legs();
    echo "<br>";
    echo $sungokong->get_cold_blooded();
    echo "<br>";
    $sungokong->yell(); // "Auooo"

    echo "<br><br>";

    $kodok = new Frog("buduk");

    echo $kodok->get_name();
    echo "<br>";
    echo $kodok->get_legs();
    echo "<br>";
    echo $kodok->get_cold_blooded();
    echo "<br>";
    $kodok->jump(); // "hop hop"
   